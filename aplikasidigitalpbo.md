# 1. Mampu mendemonstrasikan penyelesaian masalah dengan pendekatan matematika dan algoritma pemrograman secara tepat.

~~~~
import java.util.ArrayList;
import java.util.Scanner;

abstract class Upload {
    protected String nama;
    protected String jenis;
    // Mengubah tipe data dari files menjadi ArrayList<File>
    protected ArrayList<File> files;
    
    public Upload(String nama, String jenis) {
        this.nama = nama;
        this.jenis = jenis;
        this.files = new ArrayList<>();
    }

    // Getter dan setter untuk nama
    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
    this.nama = nama;
}

    // Getter dan setter untuk jenis
    public String getJenis() {
        return jenis;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

    // Metode abstrak yang harus diimplementasikan oleh kelas turunan
    public abstract String toString();
}

    class File extends Upload {
        public File(String nama, String jenis) {
            super(nama, jenis);
        }
    // Polimorfisme saat memanggil metode toString() pada class file
    public String toString() {
        return "Nama File: " + nama + "\nJenis File: " + jenis + "\n";
    }
}

    class Folder extends Upload {
        public Folder(String nama) {
            super(nama, "Folder");
        }

    public ArrayList<File> getFiles() {
        return files;
    }

    public void addFile(File file) {
        files.add(file);
    }

    public void removeFile(File file) {
        files.remove(file);
    }
    // Implementasi metode toString() pada kelas Folder
    // Polimorfisme saat memanggil metode toString() pada class folder
    public String toString() {
        String output = "Folder name: " + nama + "\n";
        for (File file : files) {
            output += file.toString();
        }
        return output;
    }
}

    public class GDrive {
        public static void main(String[] args) {
            Scanner sc = new Scanner(System.in);

            System.out.print("Masukkan Nama Pengguna: ");
            String namaPengguna = sc.nextLine();
            Folder folder = new Folder("File Ku");
            boolean isRunning = true;

            while (isRunning) {
                System.out.println("===Program Sederhana Gdrive===");
                System.out.println("Selamat datang, " + namaPengguna + "!");
                System.out.println("\nMenu:");
                System.out.println("1. Buat Sebuah File");
                System.out.println("2. Edit Jenis File");
                System.out.println("3. Hapus File");
                System.out.println("4. Tampilkan File");
                System.out.println("5. Ubah Nama File");
                System.out.println("6. Bagikan File");
                System.out.println("7. Exit");
                
                System.out.print("Pilih Menu: ");
                int menu = sc.nextInt();
                sc.nextLine();

                switch (menu) {
                    case 1:
                    System.out.print("Masukkan Nama File: ");
                    String fileNama = sc.nextLine();

                    System.out.print("Masukkan Jenis File (PDF/WORD): ");
                    String fileContent = sc.nextLine();

                    File newFile = new File(fileNama, fileContent);
                    folder.addFile(newFile);

                    System.out.println("File Telah Dibuat!");
                    break;

                    case 2:
                    System.out.print("Masukkan Nama File: ");
                    String editFileName = sc.nextLine();
                    
                    boolean isFileExist = false;
                    for (File file : folder.getFiles()) {
                        if (file.getNama().equals(editFileName)) {
                    
                            System.out.print("Masukan Jenis File Baru(PDF/WORD): ");
                            String newFileContent = sc.nextLine();

                            file.setJenis(newFileContent);

                            System.out.println("File Telah Diupdate!");
                            isFileExist = true;
                            break;
                        }
                    }

                    if (!isFileExist) {
                        System.out.println("File Tidak Ditemukan!");
                    }
                    break;

                    case 3:
                    System.out.print("Masukkan Nama File: ");
                    String deleteFileName = sc.nextLine();

                    boolean isFileDeleted = false;
                    for (File file : folder.getFiles()) {
                        if (file.getNama().equals(deleteFileName)) {
                            folder.removeFile(file);
                            System.out.println("File Berhasil Dihapus");
                            isFileDeleted = true;
                            break;
                        }
                    }
                    if (!isFileDeleted) {
                        System.out.println("File Tidak Ditemukan!");
                    }
                    break;
                    case 4:
                   
                    System.out.println(folder);
                    for (File file : folder.getFiles()) {
                        System.out.println(file.getNama() + "." + file.getJenis());
                    }
                    break;

                    case 5:
                    System.out.print("Masukkan Nama File: ");
                    String oldFileName = sc.nextLine();

                    boolean isOldFileExist = false;
                    for (File file : folder.getFiles()) {
                        if (file.getNama().equals(oldFileName)) {
                            System.out.print("Masukkan Nama File Baru: ");
                            String newFileName = sc.nextLine();

                            file.setNama(newFileName);

                            System.out.println("Nama File Telah Diubah!");
                            isOldFileExist = true;
                            break;
                        }
                    }

                    if (!isOldFileExist) {
                        System.out.println("File Tidak Ditemukan!");
                    }
                    break;

                    case 6:
                    System.out.print("Masukkan Nama File: ");
                    String shareFileName = sc.nextLine();

                    boolean isFileShared = false;
                    for (File file : folder.getFiles()) {
                        if (file.getNama().equals(shareFileName)) {
                            System.out.print("Masukkan Nama Pengguna untuk Berbagi: ");
                            String shareUserName = sc.nextLine();

                            // Logika untuk berbagi file dengan pengguna lain

                            System.out.println("File Telah Dibagikan!");
                            isFileShared = true;
                            break;
                        }
                    }

                    if (!isFileShared) {
                        System.out.println("File Tidak Ditemukan!");
                    }
                    break;

                    case 7:
                    System.out.println("Terima kasih telah menggunakan program ini!");
                    isRunning = false;
                    break;

                    default:
                    System.out.println("Menu Tidak Valid!");
                    break;
                }
            }
        }
    }

~~~~

Program tersebut menggunakan pendekatan objek dan menu interaktif untuk mengelola file dalam sebuah folder. Dengan program tersebut, pengguna dapat melakukan berbagai operasi seperti membuat file baru, mengedit jenis file, menghapus file, menampilkan daftar file, mengubah nama file, dan membagikan file. Program ini menggunakan struktur data berupa kelas-kelas yang merepresentasikan file dan folder, serta menggunakan objek Scanner untuk menerima input dari pengguna melalui keyboard.

# 2. Mampu menjelaskan algoritma dari solusi yang dibuat

1. Mulai dengan mengimpor pustaka yang diperlukan, yaitu `java.util.ArrayList` dan `java.util.Scanner`.
2. Buat kelas abstrak bernama `Upload` dengan beberapa informasi seperti nama, jenis, dan daftar file yang disimpan dalam bentuk ArrayList.
3. Buat sebuah konstruktor untuk kelas `Upload` yang menerima nama dan jenis sebagai parameter, dan inisialisasikan properti-properti tersebut.
4. Buatlah metode getter dan setter untuk properti nama dan jenis.
5. Terdapat sebuah metode abstrak bernama `toString()` yang harus diimplementasikan oleh kelas turunan.
6. Buat kelas `File` yang merupakan turunan dari `Upload`.
7. Buatlah konstruktor untuk kelas `File` yang menerima nama dan jenis sebagai parameter dan memanggil konstruktor `Upload` dengan nilai-nilai yang sesuai.
8. Override metode `toString()` untuk mengembalikan informasi tentang nama dan jenis file.
9. Buat kelas `Folder` yang merupakan turunan dari `Upload`.
10. Buatlah konstruktor untuk kelas `Folder` yang menerima nama sebagai parameter dan memanggil konstruktor `Upload` dengan nilai-nilai yang sesuai.
11. Buat metode `getFiles()` untuk mengembalikan daftar file dalam folder.
12. Buat metode `addFile()` untuk menambahkan file ke dalam folder.
13. Buat metode `removeFile()` untuk menghapus file dari folder.
14. Override metode `toString()` untuk mengembalikan informasi tentang nama folder dan daftar file yang ada di dalamnya.
15. Buat kelas `GDrive` yang berisi metode `main()` sebagai titik masuk program.
16. Buat objek `Scanner` untuk menerima masukan dari pengguna.
17. Tanyakan kepada pengguna nama pengguna dan simpan nilainya.
18. Buat objek `Folder` dengan nama "File Ku".
19. Buat variabel `isRunning` dengan nilai `true` untuk menjaga program tetap berjalan.
20. Dalam loop `while`, tampilkan menu kepada pengguna dan terima pilihan menu.
21. Gunakan pernyataan switch case untuk menangani setiap pilihan menu.
22. Untuk setiap pilihan menu, lakukan operasi yang sesuai, seperti membuat file baru, mengedit jenis file, menghapus file, menampilkan daftar file, mengubah nama file, atau membagikan file.
23. Jika pilihan menu adalah 7, tampilkan pesan penutup dan ubah nilai `isRunning` menjadi `false` untuk menghentikan program.
24. Jika pilihan menu tidak valid, tampilkan pesan kesalahan.
25. Ulangi langkah 20-24 selama program masih berjalan.
26. Selesai.

# 3. Mampu menjelaskan konsep dasar OOP

Objek dalam OOP memiliki 4 pilar, diantaranya :

- Encapsulation: Kemampuan untuk menyembunyikan data dan implementasi dalam sebuah objek dan hanya memberikan akses melalui metode atau antarmuka yang didefinisikan.
- Inheritance: Kemampuan untuk membuat objek baru yang memiliki karakteristik dari objek yang sudah ada, sehingga memungkinkan penggunaan kembali kode dan mempermudah pengorganisasian kode.
- Polymorphism: Kemampuan untuk memungkinkan objek untuk memiliki banyak bentuk atau perilaku yang berbeda tergantung pada konteks penggunaannya.
- Abstraction: kemampuan untuk mengisolasi detail yang tidak diperlukan dan hanya mengekspos informasi yang relevan atau penting.

# 4. Mampu mendemonstrasikan penggunaan Encapsulation secara tepat

~~~~
Abstract class Upload {
    protected String nama;
    protected String jenis;
    // Mengubah tipe data dari files menjadi ArrayList<File>
    protected ArrayList<File> files;

// Encapsulation: Getter dan setter untuk properti nama
    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    // Encapsulation: Getter dan setter untuk properti jenis
    public String getJenis() {
        return jenis;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }
~~~~
~~~~
 // Encapsulation: Getter untuk properti files
    public ArrayList<File> getFiles() {
        return files;
    }

    // Encapsulation: Menambahkan file ke dalam folder
    public void addFile(File file) {
        files.add(file);
    }

    // Encapsulation: Menghapus file dari folder
    public void removeFile(File file) {
        files.remove(file);
    }
~~~~

Terdapat penggunaan access modifiers (protected, private) pada atribut dan metode untuk mengatur tingkat akses ke data dan fungsionalitas dalam kelas.
Dalam kelas Upload, atribut nama dan jenis dideklarasikan sebagai protected, yang berarti hanya dapat diakses oleh kelas turunan.
Metode getter dan setter digunakan untuk mengakses dan mengubah nilai atribut dengan menggunakan prinsip enkapsulasi, sehingga data tetap terlindungi dan kontrol diberikan melalui metode.

# 5. Mampu mendemonstrasikan penggunaan Abstraction secara tepat 
~~~~
// Abstraction: Kelas abstrak yang digunakan sebagai dasar untuk kelas turunan
abstract class Upload {
    protected String nama;
    protected String jenis;
    protected ArrayList<File> files;

    public Upload(String nama, String jenis) {
        this.nama = nama;
        this.jenis = jenis;
        this.files = new ArrayList<>();
    }
~~~~
Kelas Upload adalah kelas abstrak, ditandai dengan kata kunci abstract.
Kelas abstrak tidak dapat diinstansiasi langsung, tetapi hanya dapat diwarisi oleh kelas turunannya.
Kelas abstrak dapat memiliki metode abstrak (tanpa implementasi) yang harus diimplementasikan oleh kelas turunannya.
Dalam kodingan ini, metode toString() dalam kelas Upload dideklarasikan sebagai metode abstrak yang harus diimplementasikan oleh kelas turunan (File dan Folder).

# 6. Mampu mendemonstrasikan penggunaan Inheritance dan Polymorphism secara tepat

~~~~
// Inheritance: Kelas turunan dari kelas Upload
class File extends Upload {
    public File(String nama, String jenis) {
        super(nama, jenis);
    }

    public String toString() {
        return "Nama File: " + nama + "\nJenis File: " + jenis + "\n";
    }
}

// Inheritance: Kelas turunan dari kelas Upload
class Folder extends Upload {
    public Folder(String nama) {
        super(nama, "Folder");
    }
~~~~
Dalam kodingan ini, terdapat pewarisan antara kelas File dan Folder dengan kelas Upload sebagai kelas induknya.
Kelas File dan Folder mewarisi atribut dan metode dari kelas Upload.
Dalam pewarisan, kelas anak (turunan) dapat menggunakan dan memperluas fungsionalitas yang diberikan oleh kelas induk (superclass).

~~~~
    // Polimorfisme saat memanggil metode toString() pada class file
    public String toString() {
        return "Nama File: " + nama + "\nJenis File: " + jenis + "\n";
    }
}

    // Implementasi metode toString() pada kelas Folder
    // Polimorfisme saat memanggil metode toString() pada class folder
    public String toString() {
        String output = "Folder name: " + nama + "\n";
        for (File file : files) {
            output += file.toString();
        }
        return output;
    }
~~~~

Dalam kodingan ini, polimorfisme terlihat dalam metode toString().
Metode toString() di-override di kelas File dan Folder sesuai dengan kebutuhan masing-masing kelas.
Polimorfisme memungkinkan kita untuk menggunakan metode toString() yang sama namun memberikan implementasi yang berbeda tergantung pada objek yang digunakan.

# 7. Mampu menerjemahkan proses bisnis ke dalam skema OOP secara tepat

Identifikasi entitas utama: Identifikasi entitas utama yang terlibat dalam proses bisnis. Misalnya, dalam kasus pengelolaan file dan folder, entitas utama dapat berupa File, Folder, dan User.

Definisikan kelas-kelas: Buat kelas-kelas yang mewakili entitas-entitas tersebut. Setiap kelas harus memiliki atribut dan metode yang relevan dengan entitas yang diwakilinya.

Identifikasi hubungan antar entitas: Tentukan hubungan antar entitas-entitas tersebut. Misalnya, User dapat memiliki akses ke File atau Folder tertentu, sehingga ada hubungan antara kelas User dengan kelas File dan Folder.

Definisikan atribut dan metode: Untuk setiap kelas, tentukan atribut-atribut yang diperlukan untuk menyimpan informasi dan metode-metode yang memungkinkan manipulasi dan interaksi antar objek.

Implementasikan metode: Implementasikan kode untuk setiap metode yang ada dalam kelas-kelas tersebut. Pastikan metode-metode ini mencerminkan perilaku yang diharapkan sesuai dengan proses bisnis yang ingin diwakili.

Buat objek: Buat objek-objek dari kelas-kelas yang telah didefinisikan. Objek-objek ini akan mewakili instance konkret dari entitas-entitas dalam proses bisnis.

Gunakan objek untuk menjalankan proses bisnis: Gunakan objek-objek yang telah dibuat untuk menjalankan proses bisnis. Panggil metode-metode yang sesuai pada objek-objek tersebut untuk melakukan operasi yang diperlukan.

# 8. Mampu menjelaskan rancangan dalam bentuk Class Diagram, dan Use Case table 

- Class Diagram

![dokumentasi](dokumentasi/USD.jpeg)

- Use Case Table
![dokumentasi](dokumentasi/Table.jpeg)
# 9. Mampu memberikan gambaran umum aplikasi kepada publik menggunakan presentasi berbasis video

https://youtu.be/GPDsR3O9Dhc

# 10. Inovasi UX 

- Membuat File

![dokumentasi](dokumentasi/1.jpeg)

- Edit Jenis File

![dokumentasi](dokumentasi/2.jpeg)

- Hapus File

![dokumentasi](dokumentasi/3.jpeg)

- Tampilkan File

![dokumentasi](dokumentasi/4.jpeg)

- Ubah Nama File

![dokumentasi](dokumentasi/5.jpeg)

- Bagikan File

![dokumentasi](dokumentasi/6.jpeg)

- Exit

![dokumentasi](dokumentasi/7.jpeg)
